import requests
import validators
from django.template.defaultfilters import slugify
from bs4 import BeautifulSoup
from django_rq import job, enqueue
from core.shortener import Short
from core.models import Post, SourceUrl


def string(strn):
    """
    método que elimina los espacios y separa el title de summary
    elimina los espacion antes y despues del ultimo caracter

    :param strn: string
    """
    strn = strn.strip()
    # busca los dos puntos retornando la posicion donde lo encontro
    x = strn.find(':')
    # si es la segunda posicion indica tiene que eliminar la hora
    if int(x) == 2:
        # corta la hora
        strn = strn[5:]
    # si no llego al final de la cadena
    elif int(x) != -1:
        # corta de los dos puntos hacia atras
        strn = strn[0:int(x) - 2]
    strn = strn.strip()
    return strn


@job
def boot(source):
    """

    :return:
    """
    r = requests.get(source.url)
    if r.status_code != 200:
        return
    soup = BeautifulSoup(r.text, 'html.parser')
    # get data by class main from source url
    links = soup.find_all(True, {'class': [source.metadata['parent']]}, limit=source.metadata['size'], recursive=True)
    # iter result find
    for l in links:
        post = {}
        # Apply rules for source url
        if not source.metadata:
            continue
        for key, value in source.metadata.items():
            # Exclude rules parent and size
            if key != 'parent' and key != 'size':
                # Extracting data by rules and search
                for a in l.find_all(value[0]):
                    # Build post by rules keys
                    if value[1] == 'get':
                        post[key] = a.get(value[2])
                    elif value[1] == 'text':
                        post[key] = a.text
        if validators.url(post['link']):
            if requests.get(post['link']).status_code != 200:
                continue
        post['link'] = Short.shorter(post['link'])
        # default url image
        img_default = 'https://dummyimage.com/600x400/ede6ed/1b1c1f.png&text=FeedFancy'
        if validators.url(post['img']):
            if requests.get(post['img']).status_code == 200:  # check if img url is valid
                img_default = post['img']  # add image url
        try:
            post = Post.objects.create(
                title=string(post['title']),
                link=post['link'],
                imgUrl=img_default,
                summary=string(post['summary']),
                slug=slugify(post['title']),
                source=source.source
            )
            for t in source.taxonomies.all():
                post.taxonomies.add(t)
        except:
            pass

@job
def send_spider():
    """
    Get source url periodic and enqueue for get data with the job boot
    :return:
    """
    uri = SourceUrl.objects.filter(isActive=True)
    for u in uri:
        if u.metadata is None or u.url is None:
            continue
        enqueue(boot, u)