import django_filters.rest_framework as drf_filters
from rest_framework import filters
from rest_framework.generics import ListAPIView
from core.filters import PostFilter
from core.serializers import PublicPostSerializer
from core.models import Post


class PublicPostListView(ListAPIView):
    serializer_class = PublicPostSerializer
    filter_class = PostFilter
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, drf_filters.DjangoFilterBackend)
    ordering_fields = ('id', 'slug', 'title', 'createdAt', 'updatedAt')
    search_fields = ('id', 'title', 'summary', 'content', 'taxonomies__name')
    queryset = Post.objects.all()
